const express = require('express');
const app = express();

const hostname = '10.0.11.32';
const port = 8000;

app.get('/', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.send(`
    <html>
      <head>
        <title>Simple Test</title>
        <style>
          body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 10;
            font-family: Arial, sans-serif;
            background-color: #f5f5f5; /* Off-white background color */
          }
          h1 {
            text-align: center;
            margin-bottom: 40px; /* Space between text and buttons */
          }
          button {
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            margin: 5px;
            cursor: pointer;
          }
        </style>
        <script>
          function changeColor(color) {
            document.getElementById('text').style.color = color;
          }

          function resetColor() {
            document.getElementById('text').style.color = 'black';
          }
        </script>
      </head>
      <body>
        <div>
          <h1 id="text">Change Text Color</h1>
          <button style="background-color: red; color: white;" onclick="changeColor('red')"> Red Text</button>
          <button style="background-color: green; color: white;" onclick="changeColor('green')">Green Text</button>
          <button style="background-color: black; color: white;" onclick="resetColor()">Black Text </button>
        </div>
      </body>
    </html>
  `);
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
